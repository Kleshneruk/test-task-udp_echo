#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <unistd.h>
#include <sys/select.h>

#define MEMSIZE 512 //Max size of memory array
#define IDLEN 64	//Max length of buffer
#define PORT 7777	//The port on which to send data

struct list
{
    int field;
    struct list *ptr;
};

void die(char *s)
{
	perror(s);
	exit(EXIT_FAILURE);
}

//search ID in array memory. 
//If ID was not found in the array, it is added to the array and displays a message about the new element
int memsearch(char **memory, int size, char* ID)
{
    int isIDnew = 1;
    int a = 1;
    int endoflist = 0;
    if(ID == "")
    {
        return 0;
    }
    for(int i = 0; i < size; i++)
    {
        if(strcmp(memory[i], "") == 0)
        {
            endoflist = i;
            break;
        }
        if(strcmp(memory[i], ID) == 0)
        {
            isIDnew = 0;
            break;
        }
    }
    if(isIDnew)
    {
        strcpy(memory[endoflist], ID);
        printf("found  host %s\n", ID);
    }
    return 0;
}

int main(int argc, char **argv)
{
    struct ifaddrs *ifaddr, *ifa;
	int sock, i, selstate, family, status;
	char buf[IDLEN];
	char hostname[IDLEN];
    char host[NI_MAXHOST];
    //setup the memory array
    char **memory = (char**)malloc(512*sizeof(char*));
    for(i = 0; i < 512; i++)
    {
        memory[i] = (char*)malloc(IDLEN*sizeof(char));
        strcpy(memory[i], "");
    }
    
    if(argc != 2)
    {
        printf("Wrong argument count: only one interface name required\n");
        return 0;
    }

    //get name of host machine
    if(gethostname(hostname, sizeof(hostname)) == -1)
    {
        die("gethostname");
    }

    if (getifaddrs(&ifaddr) == -1) 
    {
        perror("getifaddrs");
        exit(EXIT_FAILURE);
    }

    //search for specified interface
    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) 
    {
        if (ifa->ifa_addr == NULL)
            continue;  

        status = getnameinfo(ifa->ifa_addr, sizeof(struct sockaddr_in), host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);

        if((strcmp(ifa->ifa_name, argv[1]) == 0) && (ifa->ifa_addr->sa_family == AF_INET))
        {
            if (status != 0)
            {
                printf("getnameinfo() failed: %s\n", gai_strerror(status));
                exit(EXIT_FAILURE);
            }
            break;
        }
    }

    //set sockaddr_in struct for binding socket on send
    struct sockaddr_in hostaddr;
    hostaddr.sin_family = AF_INET;
    hostaddr.sin_port = htons(PORT);
    hostaddr.sin_addr.s_addr = inet_addr(host);

    //set sockaddr_in struct for binding socket on recieve
    struct sockaddr_in hostaddr2;
    hostaddr2.sin_family = AF_INET;
    hostaddr2.sin_port = htons(PORT);
    hostaddr2.sin_addr.s_addr = htonl(INADDR_BROADCAST);

    //initialize timeval struct for select timeout
    struct timeval rtimeout;

    //set sockaddr_in struct for sendto
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(PORT);
    addr.sin_addr.s_addr = htonl(INADDR_BROADCAST);

    int a = 1;
	while(a)
	{
		//set timeout to 5 sec
        selstate = 0;
        rtimeout.tv_sec = 5;
        rtimeout.tv_usec = 0;
        
		//set up socket to send
        if((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
	    {
		    die("socket");
	    }

        int broadcastEnable = 1;
        if(setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &broadcastEnable, sizeof(broadcastEnable)))
        {
            die("setsockopt");
        }

        if (bind(sock, (struct sockaddr *)&hostaddr, sizeof(hostaddr)) < 0)
        {
            die("bind");
        }

		//send the message
        if(sendto(sock, hostname, strlen(hostname), 0, (struct sockaddr *)&addr, sizeof(struct sockaddr_in)) < 0)
        {
            die("sendto()");
        }
        close(sock);
		
        //set up socket to recieve
        if((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
	    {
		    die("socket");
	    }

        broadcastEnable = 1;
        if(setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &broadcastEnable, sizeof(broadcastEnable)))
        {
            die("setsockopt");
        }
        if(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &broadcastEnable, sizeof(broadcastEnable)))
        {
            die("setsockopt");
        }    

        if (bind(sock, (struct sockaddr *)&hostaddr2, sizeof(hostaddr2)) < 0)
        {
            die("bind");
        }
    
		//set up for select
        fd_set rfds;
        FD_ZERO(&rfds);
        FD_SET(sock, &rfds);

		//recieve the message
        while(rtimeout.tv_sec > 0 || rtimeout.tv_usec > 0)
        {
		    //clear the buffer by filling null
		    memset(buf,'\0', IDLEN);
		    //try to receive package        
            selstate = select(sock+1, &rfds, NULL, NULL, &rtimeout);
            switch(selstate)
            {
                case(0):
                {
                    break;
                }
                case(-1):
                {
                    perror("select()");
                    break;
                }
                default:
                {
                    if (recvfrom(sock, buf, IDLEN, 0, NULL, 0) == -1)
	                {
		                die("recvfrom()");
	                }
                    else
                    {
                        memsearch(memory, 512, buf);
                    }
                }
            }
        }
        close(sock);
	}

	close(sock);
	return 0;
}
