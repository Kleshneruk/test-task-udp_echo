# UDP client test task in C++
## Description
This program is the result of a test task. 
- The program sends a UDP packet every five seconds from the specified interface from port 7777. 
- The packet is sent to the broadcast address and contains the host ID (hostname). 
- If a packet is received on port 7777, the ID is parsed and compared with previous received ID. 
- If a packet is received with a previously unseen ID, the message "found host %s\n" is displayed.
## Compilation
To compile the project use the Makefile
```sh
make -f Makefile
```
or the line below
```sh
gcc client.cpp -o client
```
## Program launch
To run the program, enter the following line, where Interfacename is the name of the interface that the program will use
```sh
./client Interfacename
```
